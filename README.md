# Importmod
![pipeline](https://gitlab.com/portmod/portmod-migrate/badges/master/pipeline.svg)
![coverage](https://gitlab.com/portmod/portmod-migrate/badges/master/coverage.svg)

A cli tool to help migrating from manually installed mods to the Portmod system.
